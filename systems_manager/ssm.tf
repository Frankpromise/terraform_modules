resource "aws_ssm_document" "example" {
  name          = "ssm-document"
  document_type = "Command"

  content = jsonencode({
    schemaVersion = "1.2"
    description   = "Example SSM document to run a shell command"
    parameters = {
      command = {
        type = "String"
        default = "echo 'Hello, World!'"
      }
    }
    runtimeConfig = {
      "aws:runShellScript" = {
        properties = {
          id         = "0.aws:runShellScript"
          runCommand = [
            "echo 'Hello, World!'"
          ]
        }
        }
      }
    
  })
   lifecycle {
    ignore_changes = [content]
  }
}




resource "aws_ssm_association" "example" {
  for_each           = local.instances_map
  name        = aws_ssm_document.example.name
  instance_id = each.value.id

}


resource "null_resource" "send_command" {
  for_each = local.instances_map
  triggers = {
    ssm_document = aws_ssm_document.example.id
    instance_id = each.key
  }

  provisioner "local-exec" {
    command = "aws ssm send-command --document-name ${aws_ssm_document.example.name} --instance-ids ${each.key}"
  }
}
