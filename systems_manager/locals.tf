locals {
  instances_map = { for idx, instance in var.instance : "instance-${idx}" => instance }
}
