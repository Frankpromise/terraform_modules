provider "aws" {
    region = "eu-west-1"
}


terraform {
  backend "s3" {
    bucket = "terraform-state-bucket-gitlab"  
    key    = "terraform-state-file.tfstate"
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}