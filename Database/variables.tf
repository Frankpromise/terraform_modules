#----database/variables---

variable "db_storage" {
  
}

variable "db_instance_class" {
  
}

variable "dbname" {
  
}

variable "db_engine_version" {
  
}

variable "db_identifier" {
  
}


variable "db_subnet_group_name" {
  
}

variable "dbpass" {
  
}

variable "dbuser" {
  
}

variable "vpc_security_group_ids" {
  
}

variable "skip_final_snapshot" {
  
}