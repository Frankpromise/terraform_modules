variable "instance_count" {

}

variable "instance_type" {
  
}

variable "vol_size" {
  
}
variable "public_sg" {
  
}

variable "public_subnets" {
  
}

variable "key_name" {
  default = "my-key"

}



variable "lb_target_group_arn" {
  
}

variable "tg_port" {
  
}

variable "user_data_path" {


}

variable "route53" {
  
}

variable "private_zone" {
  type = bool
}

variable "record_name" {
  
}

variable "type" {
  
}

variable "ttl" {
  
}
