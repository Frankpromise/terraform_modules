#---compute/outputs---

output "instance" {
  value = aws_instance.mtc_node[*]
  
}


output "instance_port" {
  value = aws_lb_target_group_attachment.mtc_tg_attach[0].port
}

output "key_pair" {
  description = "The SSH key pair information"
  value       = data.aws_key_pair.ssm
}

output "public_ip" {
  value = aws_instance.mtc_node.*.public_ip
}