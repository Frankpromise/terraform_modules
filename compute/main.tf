#---compute/main.tf----

data "aws_ami" "server_ami" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
}


resource "random_id" "mtc_node_id" {
  byte_length = 2
  count = var.instance_count
  keepers = {
    "key_name" = var.key_name
  }
}

/* resource "aws_key_pair" "mtc_auth" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
} */

data "aws_key_pair" "ssm" {
  key_name = "access"
}


resource "aws_instance" "mtc_node" {
  count = var.instance_count
  instance_type = var.instance_type
  ami = data.aws_ami.server_ami.id
  iam_instance_profile = aws_iam_instance_profile.ssm.name

  tags = {
    Name = "mtc_node-${random_id.mtc_node_id[count.index].dec}"
  }


key_name = data.aws_key_pair.ssm.key_name
vpc_security_group_ids = [var.public_sg]
subnet_id = var.public_subnets[count.index]
user_data              = file(var.user_data_path)
root_block_device{
    volume_size = var.vol_size
}
}
/* provisioner "remote-exec" {
   connection {
    type = "ssh"
    user = "ubuntu"
    host = self.public_ip
    private_key = file("/home/ubuntu/.ssh/mtckey")
   }
} */
#provisioner "local-exec" {
  #command = templatefile("${path.cwd}/script.tpl")
#}
resource "aws_lb_target_group_attachment" "mtc_tg_attach" {
  count = var.instance_count
  target_group_arn = var.lb_target_group_arn
  target_id = aws_instance.mtc_node[count.index].id
  port = var.tg_port
}

data "aws_route53_zone" "route53" {
  name         = var.route53
  private_zone = var.private_zone
}

#  create Route53 table record 

resource "aws_route53_record" "domainName" {
  name    = var.record_name
  type    = var.type
  zone_id = data.aws_route53_zone.route53.zone_id
  records = local.records
  ttl     = var.ttl
  depends_on = [ aws_instance.mtc_node ]
}